module.exports = {
  siteMetadata: {
    title: `react app`,
    siteUrl: `https://www.yourdomain.tld`
  },
  plugins: ["gatsby-plugin-sass"]
};
