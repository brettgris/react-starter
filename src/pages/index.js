import * as React from "react"
import { Link } from 'gatsby';
import { Footer } from '../components/footer/footer';

import '../styles/index.scss';

const Index = () => (
  <div>
    <header>
      <Link to="/about">About</Link>
    </header>
    <main>
      <h1 className="index-title">Index Page</h1>
    </main>
    <Footer />
  </div>
);

export default Index;
