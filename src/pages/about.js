import * as React from "react"
import { Link } from 'gatsby';
import { Footer } from '../components/footer/footer';

import '../styles/about.scss';

const About = () => (
  <div>
    <header>
      <Link to="/">Home</Link>
    </header>
    <main>
      <h1 className="about-title">About Page</h1>
    </main>
    <Footer />
  </div>
);

export default About

