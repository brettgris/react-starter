# Setup

### NPM

First, we need to make sure your computer is setup to use npm. To do this open terminal either from your applications folder or from spotlight search by typing terminal. 

Once terminal is open type `npm -v`, if you get an output of some numbers such as `6.14.17`, please skip to install repo. If you do not, please install Node from https://nodejs.org/en/ and follow install instructions there. Once that is done, try to repeat this step.


### Download Files

Next, click the download button which is the next to the 'Clone' button. Feel to clone if you are comfortable with using 'git'. Once you have the files, we need to do open up that folder in terminal. Easiest way to do that is drag the folder to the terminal app in your dock. You can also open using file open or `cd` command to it. Once you have the name of this folder showing in terminal, proceed to the next step.

### Run Install

Next, you need to install packages required for this to run. From terminal inside project folder, run `npm install`

### Start Project

After that is completed, you are able to start your project. From terminal inside project folder, run `npm run develop`

### Launch Browser

Navigate to http://localhost:8000/

# Pages

With gatbsy, js files that are placed in the `src/pages` directory act as the pages that you can navigate to. Other js elements should be placed in the components folder.

# SCSS or CSS Files

Be sure to include these files like the example or by importing them in a js file `import "index.scss"`

## Additional Resources

* https://sass-lang.com/ - scss documentation
* https://www.gatsbyjs.com/docs/how-to - this is a build time compilier for react that handles a lot 
* https://indeed.udemy.com/organization/search/?kw=react&q=react&src=sac - Udemy courses for react - 
